from http.client import BAD_GATEWAY
import couchdb
import json

#seleccionar servidor
couch=couchdb.Server('http://localhost:5984/') #server(http://usuario:contraseña@host:puerto)
#otra forma de pasar credenciales
couch.resource.credentials= ("admin","12345") #(nombre, contraseña)

couch.delete('test') #para eliminar bases de datos

#crear o selleccionar base de datos existente
db=couch.create('test') #crear
#db=couch['test'] #seleccionar

#valor/documento a guardar
dc={"_id":"tomate","vegetal":True}
dc2={"_id":"salmon","vegetal":False}
#guardar el documento en la base
db.save(dc)
db.save(dc2)
#guarda los datos del documento en la misma variable
print()
print(dc)
print(dc2)
print()

#conseguir documento mediante id
dc= db["tomate"]
print(dc["_id"])
print(dc["vegetal"])
#devuelve los campos junto con los campos especiales de _id y _rev(revision/version)
#DEVUELVE EN FORMA DE DICT, NO JSON
#aquellos campos/bd que empiecen con _ son campos especiales

#encuentra todos los documentos de la BD e imprime la id
print("\nIDs:")
for id in db:
    print (id)


#Pruebas
V=0
Ca=0
for vegetal in db:
    X=+1
    print(X)
    if vegetal=="true":
        V=+1
    else:
        Ca=+1
print("\nVegetales: "+str(V))
print("Carnes: "+str(Ca))